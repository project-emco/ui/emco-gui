//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================
import React, { useState } from "react";
import withStyles from "@mui/styles/withStyles";
import makeStyles from "@mui/styles/makeStyles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import DeleteDialog from "../common/Dialogue";
import Notification from "../common/Notification";
import Link from "@mui/material/Link";
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  CloudOffOutlined as CloudOffOutlinedIcon,
  BackupOutlined as BackupOutlinedIcon,
} from "@mui/icons-material";
import { Divider, Stack } from "@mui/material";
import RestService from "./restService";
import Button from "@mui/material/Button";
import ManageServiceInstanceGroup from "./ManageServiceInstanceGroup";

const StyledTableCell = withStyles(() => ({
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 350,
  },
  cell: {
    color: "grey",
  },
});

export default function ServicesInstanceGroupTable({
  serviceInstanceGroups,
  serviceInstances,
  setData,
  ...props
}) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [index, setIndex] = useState(-1);
  const [notificationDetails, setNotificationDetails] = useState({});
  const [openDrawer, setOpenDrawer] = useState(false);
  const [confirmationDetails, setConfirmationDetails] = useState({
    confirmationButtonText: "",
    conformationTitle: "",
    conformationContent: "",
  });
  const handleCloseDialog = (el) => {
    let request = {
      projectName: props.projectName,
      serviceName: serviceInstanceGroups[index].metadata.name,
    };
    if (el.target.innerText.toLowerCase() === "delete") {
      deleteService(index, request);
    } else if (el.target.innerText.toLowerCase() === "terminate") {
      terminateService(index, request);
    } else if (el.target.innerText.toLowerCase() === "ok") {
      instantiateService(index, request);
    }
    setOpen(false);
    setIndex(-1);
  };
  const handleDelete = (index) => {
    setIndex(index);
    setOpen(true);
    setConfirmationDetails({
      confirmationButtonText: "Delete",
      conformationTitle: "Delete",
      conformationContent: `Are you sure you want to delete service "${
        serviceInstanceGroups[index]
          ? serviceInstanceGroups[index].metadata.name
          : ""
      }" ?`,
    });
  };

  const handleTerminate = (index) => {
    setIndex(index);
    setOpen(true);
    setConfirmationDetails({
      confirmationButtonText: "Terminate",
      conformationTitle: "Terminate",
      conformationContent: `Are you sure you want to terminate "${
        serviceInstanceGroups[index]
          ? serviceInstanceGroups[index].metadata.name
          : ""
      }" ?`,
    });
  };

  const handleInstantiate = (index) => {
    setIndex(index);
    setOpen(true);
    setConfirmationDetails({
      confirmationButtonText: "OK",
      conformationTitle: "Instantiate",
      conformationContent: `Are you sure you want to instantiate "${
        serviceInstanceGroups[index]
          ? serviceInstanceGroups[index].metadata.name
          : ""
      }" ?`,
    });
  };

  const instantiateService = (index, request) => {
    RestService.instantiateService(request.projectName, request.serviceName)
      .then(() => {
        let updatedData = [...serviceInstanceGroups];
        updatedData[index].status.deployedStatus = "Instantiated";
        setData([...updatedData]);
        setNotificationDetails({
          show: true,
          message: `Service instance group "${serviceInstanceGroups[index].metadata.name}" instantiated`,
          severity: "success",
        });
      })
      .catch((err) => {
        setNotificationDetails({
          show: true,
          message: `Error instantiating service instance group  : ", ${err}`,
          severity: "error",
        });
        console.log("Error instantiating service instance group  : ", err);
      });
  };

  const terminateService = (index, request) => {
    RestService.terminateService(request.projectName, request.serviceName)
      .then(() => {
        let updatedData = [...serviceInstanceGroups];
        updatedData[index].status.deployedStatus = "Terminated";
        setData([...updatedData]);
        setNotificationDetails({
          show: true,
          message: `Service "${serviceInstanceGroups[index].metadata.name}" terminated`,
          severity: "success",
        });
      })
      .catch((err) => {
        setNotificationDetails({
          show: true,
          message: `Error terminating service : ", ${err}`,
          severity: "error",
        });
        console.log("Error terminating service : ", err);
      });
  };
  const deleteService = (index, request) => {
    RestService.deleteService(request.projectName, request.serviceName)
      .then(() => {
        serviceInstanceGroups.splice(index, 1);
        setData([...serviceInstanceGroups]);
      })
      .catch((err) => {
        console.log("Error deleting service instance group  : ", err);
      });
  };
  const getDigLinks = (digs) => {
    let links = [];

    digs.forEach((dig, index) => {
      let digData = dig.split(".");
      links.push(
        <Link
          key={digData + "link" + index}
          href={`/app/projects/${digData[0]}/deployment-intent-groups/${digData[1]}/${digData[2]}/${digData[3]}/status`}
        >
          {digData[3]}
        </Link>
      );
      if (index !== digs.length - 1) {
        links.push(
          <Divider
            key={digData + "divider" + index}
            orientation="vertical"
            variant="middle"
            flexItem
          />
        );
      }
    });

    return <>{links}</>;
  };

  const handleDrawerOpen = (index) => {
    setIndex(index);
    setOpenDrawer(true);
  };

  const handleDrawerClose = () => {
    setIndex(-1);
    setOpenDrawer(false);
  };

  const updateData = (updatedEntry) => {
    const index = serviceInstanceGroups.findIndex(function (item) {
      return item.metadata.name === updatedEntry.metadata.name;
    });

    updatedEntry.status = serviceInstanceGroups[index].status;
    serviceInstanceGroups[index] = updatedEntry;
    let updatedData = [...serviceInstanceGroups];

    updatedData[index].status.deployedStatus = "Terminated";
    setData([...updatedData]);
  };

  return (
    <React.Fragment>
      <Notification notificationDetails={notificationDetails} />
      <ManageServiceInstanceGroup
        setData={setData}
        updateData={updateData}
        refreshData={props.refreshData}
        setNotificationDetails={setNotificationDetails}
        serviceInstances={serviceInstances}
        openDrawer={openDrawer}
        onDrawerClose={handleDrawerClose}
        projectName={props.projectName}
        serviceInstanceGroup={index >= 0 ? serviceInstanceGroups[index] : null}
      />
      {serviceInstanceGroups && serviceInstanceGroups.length > 0 && (
        <>
          <DeleteDialog
            confirmationText={confirmationDetails.confirmationButtonText}
            open={open}
            onClose={handleCloseDialog}
            title={confirmationDetails.conformationTitle}
            content={confirmationDetails.conformationContent}
          />
          <TableContainer component={Paper}>
            <Table className={classes.table} size="small">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Name</StyledTableCell>
                  <StyledTableCell>Description</StyledTableCell>
                  <StyledTableCell>Status</StyledTableCell>
                  <StyledTableCell>Service Instances</StyledTableCell>
                  <StyledTableCell style={{ width: "17%" }}>
                    Actions
                  </StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {serviceInstanceGroups.map((row, index) => (
                  <StyledTableRow key={row.metadata.name + "" + index}>
                    <StyledTableCell>
                      <Button
                        variant="text"
                        onClick={() => handleDrawerOpen(index)}
                      >
                        {row.metadata.name}
                      </Button>
                    </StyledTableCell>
                    <StyledTableCell className={classes.cell}>
                      {row.metadata.description}
                    </StyledTableCell>
                    <StyledTableCell className={classes.cell}>
                      {row.status.deployedStatus}
                    </StyledTableCell>
                    <StyledTableCell className={classes.cell}>
                      <Stack
                        direction="row"
                        spacing={2}
                        sx={{ maxWidth: "62rem", overflowX: "auto" }}
                      >
                        {getDigLinks(row.spec.digs)}
                      </Stack>
                    </StyledTableCell>
                    <StyledTableCell className={classes.cell}>
                      <IconButton
                        disabled={row.status.deployedStatus === "Instantiated"}
                        title="Instantiate"
                        onClick={() => handleInstantiate(index)}
                        color={"primary"}
                        size="large"
                      >
                        <BackupOutlinedIcon />
                      </IconButton>
                      <IconButton
                        disabled={row.status.deployedStatus !== "Instantiated"}
                        onClick={() => handleTerminate(index)}
                        title="Terminate"
                        color="secondary"
                        size="large"
                      >
                        <CloudOffOutlinedIcon />
                      </IconButton>
                      <IconButton
                        onClick={() => props.onEditService(index)}
                        title="Edit"
                        color="primary"
                        size="large"
                      >
                        <EditIcon />
                      </IconButton>
                      <IconButton
                        disabled={row.status.deployedStatus === "Instantiated"}
                        onClick={() => handleDelete(index)}
                        title="Delete"
                        color="secondary"
                        size="large"
                      >
                        <DeleteIcon />
                      </IconButton>
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
    </React.Fragment>
  );
}
