//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import { requestRest } from "../services/apiService";

class RestService {
  instantiateService = async (projectName, serviceName) => {
    const init = {
      url: `/v2/projects/${projectName}/services/${serviceName}/instantiate`,
      method: "POST",
    };
    const response = await requestRest(init);
    return response.data;
  };
  terminateService = async (projectName, serviceName) => {
    const init = {
      url: `/v2/projects/${projectName}/services/${serviceName}/terminate`,
      method: "POST",
    };
    const response = await requestRest(init);
    return response.data;
  };

  createService = async (projectName, servicePayload) => {
    const init = {
      url: `/v2/projects/${projectName}/services`,
      data: JSON.stringify(servicePayload),
      method: "POST",
    };
    const response = await requestRest(init);
    return response.data;
  };

  getAllServices = async (projectName) => {
    const init = {
      url: `/middleend/projects/${projectName}/service-instance-groups`,
    };
    const response = await requestRest(init);
    return response.data;
  };

  deleteService = async (projectName, serviceName) => {
    const init = {
      url: `/v2/projects/${projectName}/services/${serviceName}`,
      method: "delete",
    };
    const response = await requestRest(init);
    return response.data;
  };

  editService = async (projectName, serviceName, servicePayload) => {
    const init = {
      url: `/v2/projects/${projectName}/services/${serviceName}`,
      data: JSON.stringify(servicePayload),
      method: "PUT",
    };
    const response = await requestRest(init);
    return response.data;
  };

  addServiceInstances = async (
    projectName,
    serviceInstanceGroupName,
    servicesArray
  ) => {
    let payload = { add: servicesArray };
    const init = {
      url: `/v2/projects/${projectName}/services/${serviceInstanceGroupName}/update`,
      data: JSON.stringify(payload),
      method: "POST",
    };
    const response = await requestRest(init);
    return response.data;
  };

  removeServiceInstances = async (
    projectName,
    serviceInstanceGroupName,
    servicesArray
  ) => {
    let payload = { remove: servicesArray };
    const init = {
      url: `/v2/projects/${projectName}/services/${serviceInstanceGroupName}/update`,
      data: JSON.stringify(payload),
      method: "POST",
    };
    const response = await requestRest(init);
    return response.data;
  };

  getServiceStatus = async (projectName, serviceName) => {
    const init = {
      url: `/v2/projects/${projectName}/services/${serviceName}/status`,
      method: "GET",
    };
    const response = await requestRest(init);
    return response.data;
  };
}

export const restService = new RestService();
export default restService;
