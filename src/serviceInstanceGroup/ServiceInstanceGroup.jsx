//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================
import React, { useEffect, useState } from "react";
import ServicesTable from "./ServicesInstanceGroupTable";
import { Button, Grid, Typography } from "@mui/material";
import withStyles from "@mui/styles/withStyles";
import AddIcon from "@mui/icons-material/Add";
import apiService from "../services/apiService";
import Spinner from "../common/Spinner";
import ServiceInstanceGroupForm from "./ServiceInstanceGroupForm";
import { ReactComponent as EmptyIcon } from "../assets/icons/empty.svg";
import RestService from "./restService";

const styles = {
  root: {
    display: "flex",
    minHeight: "100vh",
  },
  app: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
  },
};

const ServiceInstanceGroup = (props) => {
  const [formOpen, setFormOpen] = useState(false);
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [digData, setDigData] = useState([]);
  const [activeServiceIndex, setActiveServiceIndex] = useState(-1);

  const serviceFormOpen = (index) => {
    if (index && index >= 0) {
      setActiveServiceIndex(index);
    } else {
      setActiveServiceIndex(-1);
    }
    setFormOpen(true);
  };

  const handleClose = () => {
    setFormOpen(false);
  };
  const onCreateService = () => {
    serviceFormOpen();
  };

  const handleEditService = (index) => {
    serviceFormOpen(index);
  };

  const handleSubmitServiceForm = async (inputFields) => {
    try {
      let requestPayload = {
        metadata: {
          name: inputFields.name,
          description: inputFields.description,
        },
        spec: { digs: inputFields.digs },
      };
      let serviceResponse;
      if (activeServiceIndex && activeServiceIndex >= 0) {
        serviceResponse = await RestService.editService(
          props.projectName,
          requestPayload.metadata.name,
          requestPayload
        );
        serviceResponse.status = await RestService.getServiceStatus(
          props.projectName,
          serviceResponse.metadata.name
        );
        data[activeServiceIndex] = serviceResponse;
        setData([...data]);
      } else {
        serviceResponse = await RestService.createService(
          props.projectName,
          requestPayload
        );
        serviceResponse.status = await RestService.getServiceStatus(
          props.projectName,
          serviceResponse.metadata.name
        );
        data && data.length > 0
          ? setData([...data, serviceResponse])
          : setData([serviceResponse]);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
      setFormOpen(false);
    }
  };

  const getServiceInstanceGroups = () => {
    return RestService.getAllServices(props.projectName);
  };
  const getDigs = async () => {
    return apiService.getDeploymentIntentGroups({
      projectName: props.projectName,
    });
  };

  async function fetchInitData() {
    try {
      let initData = await Promise.all([getDigs(), getServiceInstanceGroups()]);
      if (initData[0]) {
        setDigData(initData[0]);
      } else {
        setDigData([]);
      }
      if (initData[1]) {
        setData(initData[1].sort(sortDataByName));
      } else {
        setData([]);
      }
    } catch (err) {
      console.log(err);
    } finally {
      setIsLoading(false);
    }
  }

  const refreshServiceInstanceGroupData = async () => {
    try {
      let data = await getServiceInstanceGroups();
      if (data) {
        setData(data);
      } else {
        setData([]);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchInitData();
  }, [props.projectName]);

  const sortDataByName = (a, b) => {
    return a.metadata.name.localeCompare(b.metadata.name, undefined, {
      numeric: true,
      sensitivity: "base",
    });
  };

  return (
    <>
      {isLoading && <Spinner />}
      {!isLoading && digData && (
        <>
          <ServiceInstanceGroupForm
            item={data[activeServiceIndex] || null}
            projectName={props.projectName}
            open={formOpen}
            onClose={handleClose}
            onSubmit={handleSubmitServiceForm}
            data={{
              services: data,
              digs: digData,
              projectName: props.projectName,
            }}
          />
          <Grid item xs={12}>
            <Button
              variant="outlined"
              color="primary"
              startIcon={<AddIcon />}
              onClick={onCreateService}
            >
              Create New
            </Button>
          </Grid>

          {data && data.length > 0 && (
            <Grid container spacing={2} alignItems="center">
              <Grid item xs style={{ marginTop: "20px" }}>
                <ServicesTable
                  refreshData={refreshServiceInstanceGroupData}
                  onEditService={handleEditService}
                  serviceInstanceGroups={data}
                  serviceInstances={digData}
                  setData={setData}
                  projectName={props.projectName}
                  handleSubmitEditServiceForm={handleSubmitServiceForm}
                />
              </Grid>
            </Grid>
          )}

          {(data === null || (data && data.length < 1)) && (
            <Grid container spacing={2} direction="column" alignItems="center">
              <Grid item xs={6}>
                <EmptyIcon />
              </Grid>
              <Grid item xs={12} style={{ textAlign: "center" }}>
                <Typography variant="h5" color="primary">
                  No Service Instance
                </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                  <strong>
                    No service instance created yet, start by creating a service
                    instance
                  </strong>
                </Typography>
              </Grid>
            </Grid>
          )}
        </>
      )}
    </>
  );
};
export default withStyles(styles)(ServiceInstanceGroup);
