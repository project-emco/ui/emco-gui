//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import {
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  Drawer,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import CloseIcon from "@mui/icons-material/Close";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import { makeStyles } from "@mui/styles";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Box from "@mui/material/Box";
import * as PropTypes from "prop-types";
import Toolbar from "@mui/material/Toolbar";
import { alpha } from "@mui/material/styles";
import restService from "./restService";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import withStyles from "@mui/styles/withStyles";

const useStyles = makeStyles((theme) => ({
  cardRoot: {
    display: "flex",
    "& > *": {
      marginBottom: theme.spacing(3),
      width: "100%",
    },
  },
  cardHeader: {
    padding: "9px",
    backgroundColor: "rgba(0, 0, 0, 0.12)",
    borderRadius: "7px 7px 0 0",
    marginBottom: "10px",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}));

function EnhancedTableToolbar(props) {
  const {
    numSelected,
    onRemoveServiceInstances,
    onAddServiceInstances,
    isRemoveDisabled,
  } = props;

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: (theme) =>
            alpha(
              theme.palette.primary.main,
              theme.palette.action.activatedOpacity
            ),
        }),
      }}
    >
      {numSelected > 0 ? (
        <Typography
          sx={{ flex: "1 1 100%" }}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          sx={{ flex: "1 1 100%" }}
          variant="body1"
          id="tableTitle"
          component="div"
        >
          Service Instances
        </Typography>
      )}

      {numSelected > 0 ? (
        <Tooltip title="Remove">
          <IconButton
            onClick={onRemoveServiceInstances}
            disabled={isRemoveDisabled()}
          >
            <RemoveCircleOutlineIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Add Service Instance">
          <IconButton onClick={onAddServiceInstances}>
            <AddCircleOutlineIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  );
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const getFullNameFromServiceInstanceData = (serviceInstance, projectName) => {
  return `${projectName}.${serviceInstance.metadata.compositeAppName}.${serviceInstance.metadata.compositeAppVersion}.${serviceInstance.metadata.name}`;
};

const SelectTable = (props) => {
  const { data, selected, setSelected, projectName } = props;
  let isSelected = (name) => selected.indexOf(name) !== -1;
  const handleClick = (name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const getFullName = (serviceInstance) => {
    return getFullNameFromServiceInstanceData(serviceInstance, projectName);
  };

  return (
    <TableContainer>
      <Table size="medium">
        <TableHead>
          <TableRow>
            <StyledTableCell>Select</StyledTableCell>
            <StyledTableCell>Name</StyledTableCell>
            <StyledTableCell>Description</StyledTableCell>
            <StyledTableCell>Service</StyledTableCell>
            <StyledTableCell>State</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data &&
            data.map((row, index) => {
              const isItemSelected = isSelected(getFullName(row));
              const labelId = `service-instance-table-checkbox-${index}`;
              return (
                <TableRow
                  onClick={() => handleClick(getFullName(row))}
                  key={getFullName(row) + "-" + index}
                >
                  <StyledTableCell padding="checkbox">
                    <Checkbox color="primary" checked={isItemSelected} />
                  </StyledTableCell>
                  <StyledTableCell id={labelId} scope="row">
                    {row.metadata.name}
                  </StyledTableCell>
                  <StyledTableCell>{row.metadata.description}</StyledTableCell>
                  <StyledTableCell>
                    {row.metadata.compositeAppName}
                  </StyledTableCell>
                  <StyledTableCell>{row.spec.action}</StyledTableCell>
                </TableRow>
              );
            })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const StyledTableCell = withStyles(() => ({
  body: {
    fontSize: 14,
  },
}))(TableCell);

function AddServiceInstanceDialog(props) {
  const {
    open,
    serviceInstances,
    projectName,
    serviceInstanceGroup,
    onClose,
    onAddServiceInstances,
  } = props;

  const [selectedServiceInstance, setSelectedServiceInstance] = useState([]);
  const [availableServiceInstances, setAvailableServiceInstances] = useState(
    []
  );
  useEffect(() => {
    let asi = serviceInstances.filter((serviceInstance) => {
      let i = serviceInstanceGroup.spec.digs.indexOf(
        getFullNameFromServiceInstanceData(serviceInstance, projectName)
      );
      return i < 0;
    });
    setAvailableServiceInstances(asi);
    setSelectedServiceInstance([]);
  }, [serviceInstances, serviceInstanceGroup, projectName]);

  return (
    <Dialog fullWidth maxWidth="md" onClose={onClose} open={open}>
      <DialogTitle id="add-service-dialog-title">
        Add Service Instance(s)
      </DialogTitle>
      <DialogContent dividers>
        {availableServiceInstances.length > 0 ? (
          <SelectTable
            projectName={projectName}
            data={availableServiceInstances}
            selected={selectedServiceInstance}
            setSelected={setSelectedServiceInstance}
          />
        ) : (
          <Typography>No Service instance available to add</Typography>
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="secondary">
          Cancel
        </Button>
        <Button
          autoFocus
          color="primary"
          disabled={selectedServiceInstance.length < 1}
          onClick={() => onAddServiceInstances(selectedServiceInstance)}
        >
          Add
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const getRowsData = (serviceInstanceGroup, serviceInstances, projectName) => {
  return serviceInstances.filter((serviceInstance) => {
    let i = serviceInstanceGroup.spec.digs.indexOf(
      getFullNameFromServiceInstanceData(serviceInstance, projectName)
    );
    return i !== -1;
  });
};

export default function ManageServiceInstanceGroup({
  setData,
  setNotificationDetails,
  serviceInstances,
  projectName,
  serviceInstanceGroup,
  ...props
}) {
  const classes = useStyles();
  const [selectedServiceInstance, setSelectedServiceInstance] = useState([]);
  const [rowsData, setRowsData] = useState([]);

  const handleRemoveServiceInstances = async () => {
    try {
      const res = await restService.removeServiceInstances(
        projectName,
        serviceInstanceGroup.metadata.name,
        selectedServiceInstance
      );
      setNotificationDetails({
        show: true,
        message: `Service instance(s) removed`,
        severity: "success",
      });
      props.updateData(res);
    } catch (err) {}
  };

  const handleAddServiceInstances = async (serviceInstances) => {
    try {
      handleAddServiceFormClose();
      const res = await restService.addServiceInstances(
        projectName,
        serviceInstanceGroup.metadata.name,
        serviceInstances
      );
      setNotificationDetails({
        show: true,
        message: `Service instance(s) added`,
        severity: "success",
      });
      props.updateData(res);
    } catch (err) {}
  };

  const [addServiceFormOpen, setAddServiceFormOpen] = useState(false);

  const handleAddServiceFormClose = () => {
    setAddServiceFormOpen(false);
  };

  const handleAddServiceFormOpen = () => {
    setAddServiceFormOpen(true);
  };

  useEffect(() => {
    setSelectedServiceInstance([]);
    if (serviceInstanceGroup) {
      setRowsData(
        getRowsData(serviceInstanceGroup, serviceInstances, projectName)
      );
    }
  }, [serviceInstanceGroup, projectName, serviceInstances]);

  return (
    <Drawer anchor="right" open={props.openDrawer}>
      {serviceInstanceGroup && (
        <AddServiceInstanceDialog
          onAddServiceInstances={handleAddServiceInstances}
          open={addServiceFormOpen}
          onClose={handleAddServiceFormClose}
          serviceInstances={serviceInstances}
          projectName={projectName}
          serviceInstanceGroup={serviceInstanceGroup}
        />
      )}
      <Paper
        elevation={0}
        style={{
          width: "600px",
          padding: "5px",
          backgroundColor: "white",
          display: "flex",
          flexDirection: "column",
          height: "100%",
        }}
      >
        <DialogTitle id="manageServiceInstanceGroupDialog">
          Manage Service Instance Group
          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={props.onDrawerClose}
            size="large"
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        {serviceInstanceGroup && (
          <DialogContent dividers>
            <Box sx={{ width: "100%" }}>
              <Typography
                sx={{ marginBottom: "10px" }}
                variant="h6"
                id="tableTitle"
                component="div"
              >
                {serviceInstanceGroup.metadata.name}
              </Typography>
              <Paper>
                <EnhancedTableToolbar
                  isRemoveDisabled={() =>
                    selectedServiceInstance.length ===
                    serviceInstanceGroup.spec.digs.length
                  }
                  onAddServiceInstances={handleAddServiceFormOpen}
                  onRemoveServiceInstances={handleRemoveServiceInstances}
                  numSelected={selectedServiceInstance.length}
                />
                <SelectTable
                  projectName={projectName}
                  data={rowsData}
                  selected={selectedServiceInstance}
                  setSelected={setSelectedServiceInstance}
                />
              </Paper>
            </Box>
          </DialogContent>
        )}
        <DialogActions>
          <Button autoFocus onClick={props.onDrawerClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Paper>
    </Drawer>
  );
}
