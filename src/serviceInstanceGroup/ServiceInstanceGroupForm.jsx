//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================
import React from "react";
import PropTypes from "prop-types";
import withStyles from "@mui/styles/withStyles";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import MuiDialogTitle from "@mui/material/DialogTitle";
import MuiDialogContent from "@mui/material/DialogContent";
import MuiDialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import {
  Checkbox,
  Chip,
  FormControl,
  FormHelperText,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  TextField,
} from "@mui/material";
import * as Yup from "yup";
import { Formik, getIn } from "formik";
import LoadingButton from "../common/LoadingButton";
import Box from "@mui/material/Box";

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle className={classes.root} {...other}>
      {children}
      {onClose ? (
        <IconButton
          className={classes.closeButton}
          onClick={onClose}
          size="large"
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);
const getSchema = (existingServices) => {
  let schema;
  schema = Yup.object({
    name: Yup.string()
      .required("Name is required")
      .max(50, "Service name cannot exceed more than 50 characters")
      .matches(
        /^[a-zA-Z0-9_-]+$/,
        "Service name can only contain letters, numbers, '-' and '_' and no spaces."
      )
      .matches(
        /^[a-zA-Z0-9]/,
        "Service name must start with an alphanumeric character"
      )
      .matches(
        /[a-zA-Z0-9]$/,
        "Service name must end with an alphanumeric character"
      )
      .test(
        "duplicate-test",
        "Service with same name exists, please use a different name",
        (name) => {
          return existingServices
            ? existingServices.findIndex((x) => x.metadata.name === name) === -1
            : true;
        }
      ),
    description: Yup.string().max(
      200,
      "Service description cannot exceed more than 200 characters"
    ),
    digs: Yup.array().of(Yup.string()).required("At least one DIG is required"),
  });
  return schema;
};

const ServiceInstanceGroupForm = ({ data, ...props }) => {
  const { onClose, item, open, onSubmit } = props;
  const buttonLabel = item ? "OK" : "CREATE";
  const title = item
    ? "Edit Service Instance Group"
    : "Create Service Instance Group";
  const handleClose = () => {
    onClose();
  };
  let initialValues = item
    ? {
        name: item.metadata.name,
        description: item.metadata.description,
        digs: item.spec.digs,
      }
    : { name: "", description: "", digs: [] };

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  return (
    <Dialog
      maxWidth={"xs"}
      fullWidth
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      open={open}
    >
      <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          onSubmit(values);
        }}
        validationSchema={() => {
          if (item) {
            return getSchema();
          } else {
            return getSchema(data.services);
          }
        }}
      >
        {(props) => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit,
          } = props;

          return (
            <form noValidate onSubmit={handleSubmit}>
              <DialogContent dividers>
                <TextField
                  style={{ width: "100%", marginBottom: "10px" }}
                  id="name"
                  label="Service name"
                  type="text"
                  value={values.name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={errors.name && touched.name && errors.name}
                  required
                  disabled={item && true}
                  error={errors.name && touched.name}
                />
                <TextField
                  style={{ width: "100%", marginBottom: "25px" }}
                  name="description"
                  value={values.description}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  id="description"
                  label="Description"
                  multiline
                  maxRows={4}
                  helperText={
                    errors.description &&
                    touched.description &&
                    errors.description
                  }
                  error={errors.description && touched.description}
                />

                <FormControl
                  fullWidth
                  error={Boolean(
                    getIn(touched, "digs") && getIn(errors, "digs")
                  )}
                >
                  <InputLabel id="select-digs-label">Select DIGs</InputLabel>
                  <Select
                    labelId="select-digs-label"
                    id="digs"
                    name="digs"
                    multiple
                    value={values.digs}
                    onChange={handleChange}
                    input={<OutlinedInput label="Select DIGs" />}
                    renderValue={(selected) => (
                      <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                        {selected.map((value) => (
                          <Chip
                            key={value}
                            label={value.split(".").slice(-1)}
                          />
                        ))}
                      </Box>
                    )}
                    MenuProps={MenuProps}
                  >
                    {data.digs.map((dig) => (
                      <MenuItem
                        key={dig.metadata.name}
                        value={`${data.projectName}.${dig.metadata.compositeAppName}.${dig.metadata.compositeAppVersion}.${dig.metadata.name}`}
                      >
                        <Checkbox
                          checked={
                            values.digs.indexOf(
                              `${data.projectName}.${dig.metadata.compositeAppName}.${dig.metadata.compositeAppVersion}.${dig.metadata.name}`
                            ) > -1
                          }
                        />
                        <ListItemText primary={dig.metadata.name} />
                      </MenuItem>
                    ))}
                  </Select>
                  <FormHelperText error>
                    {getIn(touched, "digs") && getIn(errors, "digs")}
                  </FormHelperText>
                </FormControl>
              </DialogContent>
              <DialogActions>
                <Button autoFocus onClick={handleClose} color="secondary">
                  Cancel
                </Button>
                <LoadingButton
                  type="submit"
                  buttonLabel={buttonLabel}
                  loading={isSubmitting}
                />
              </DialogActions>
            </form>
          );
        }}
      </Formik>
    </Dialog>
  );
};

ServiceInstanceGroupForm.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default ServiceInstanceGroupForm;
