//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================
import React, { useEffect, useState } from "react";
import {
  Typography,
  Grid,
  Button,
  Stack,
  Divider,
  Drawer,
  IconButton,
  DialogTitle,
  Chip,
} from "@mui/material";
import ConfigureApp from "./configureApp/ConfigureApp";
import CreateIcon from "@mui/icons-material/Input";
import ListIcon from "@mui/icons-material/List";
import ConfirmationDialog from "../../common/Dialogue";
import apiService from "../../services/apiService";
import { useHistory } from "react-router-dom";
import { Edit as EditIcon } from "@mui/icons-material";
import Box from "@mui/material/Box";
import AppCard from "./AppCard";
import ActivityLog from "./ActivityLog";
import CloseIcon from "@mui/icons-material/Close";
import { getDigStatus, ServiceInstanceStatus } from "../digCommon";

function ActivityLogDrawer({
  handleActivityDrawerClose,
  openActivityLog,
  ...props
}) {
  return (
    <Drawer
      anchor="right"
      open={openActivityLog}
      onClose={handleActivityDrawerClose}
    >
      <Box
        sx={{
          width: "400px",
          backgroundColor: "#FFFFFF",
          boxShadow: "0px 3px 6px #00000029",
          borderRadius: "2px",
          height: "100%",
        }}
      >
        <DialogTitle
          color={"primary"}
          id="activityDrawer"
          onClose={handleActivityDrawerClose}
        >
          Activity Logs
          <IconButton
            aria-label="close"
            sx={{ float: "right" }}
            onClick={handleActivityDrawerClose}
            size="large"
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <Divider flexItem sx={{ borderColor: "#E6E7EA" }} />
        <Box sx={{ padding: "15px" }}>
          <ActivityLog actions={props.actions} />
        </Box>
      </Box>
    </Drawer>
  );
}
function DeploymentIntentGroupView(props) {
  const [openConfigureApp, setOpenConfigureApp] = useState(false);
  const [appToConfigure, setAppToConfigure] = useState({});
  const [openConfirmationDialog, setOpenConfirmationDialog] = useState(false);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState(getDigStatus(props.data));
  let history = useHistory();

  //if status is Deploying then again get the data after 5 secs and repeat this
  if (status === ServiceInstanceStatus.Deploying) {
    setTimeout(() => {
      props.updateData();
    }, 5000);
  }

  useEffect(() => {
    setStatus(getDigStatus(props.data));
  }, [props.data]);

  const handleCloseCheckOutDialog = (el) => {
    setLoading(true);
    if (el.target.innerText === "OK") {
      let request = {
        projectName: props.projectName,
        compositeAppName: props.compositeAppName,
        compositeAppVersion: props.compositeAppVersion,
        deploymentIntentGroupName: props.deploymentIntentGroupName,
      };
      apiService
        .checkoutServiceInstance(request)
        .then(() => {
          let path = history.location.pathname.replace("/status", "/checkout");
          history.push({
            pathname: path,
          });
        })
        .catch((err) => {
          setLoading(false);
          console.log("error in service check In", err);
          setOpenConfirmationDialog(false);
        });
    } else {
      setOpenConfirmationDialog(false);
      setLoading(false);
    }
  };
  const goToServiceCheckoutView = () => {
    let checkoutUrl = history.location.pathname.replace(
      `/${props.data["compositeApp"]}/${props.data["compositeAppVersion"]}/`,
      `/${props.data["compositeApp"]}/${props.data.targetVersion}/`
    );
    history.push(checkoutUrl.replace("/status", "/checkout"));
  };
  const [openActivityLog, setOpenActivityLog] = useState(false);
  const handleActivityDrawerClose = () => {
    setOpenActivityLog(false);
  };
  return (
    <>
      <ActivityLogDrawer
        handleActivityDrawerClose={handleActivityDrawerClose}
        openActivityLog={openActivityLog}
        actions={props.data.states.actions}
      />
      <ConfirmationDialog
        confirmationText="OK"
        open={openConfirmationDialog}
        onClose={handleCloseCheckOutDialog}
        title={"Check Out Service Instance"}
        content={`Are you sure you want to check out "${props.data.name}" ?`}
        loading={loading}
      />
      <ConfigureApp
        open={openConfigureApp}
        setOpen={setOpenConfigureApp}
        compositeAppName={props.data["compositeApp"]}
        compositeAppVersion={props.data["compositeAppVersion"]}
        app={appToConfigure}
      />

      <Box>
        <Stack spacing={3}>
          <Stack direction={"row"} spacing={2} alignItems="center">
            <Typography variant={"h5"}>{props.data.name}</Typography>
            <Chip
              sx={{ paddingLeft: "5px" }}
              size="small"
              label={status.label}
              variant="outlined"
              icon={status.icon}
              color={status.color ? status.color : undefined}
            />
            <Typography flexGrow={1} />
            {status === ServiceInstanceStatus.Deployed && (
              <>
                {props.data.isCheckedOut ? (
                  <Button
                    style={{ float: "right" }}
                    variant="outlined"
                    size="small"
                    color="primary"
                    startIcon={<EditIcon />}
                    onClick={() => goToServiceCheckoutView()}
                  >
                    Edit
                  </Button>
                ) : (
                  <Button
                    style={{ float: "right" }}
                    variant="contained"
                    size="small"
                    color="primary"
                    startIcon={<CreateIcon />}
                    onClick={() => setOpenConfirmationDialog(true)}
                  >
                    Checkout
                  </Button>
                )}
              </>
            )}
          </Stack>

          <Divider flexItem sx={{ borderColor: "#E6E7EA" }} />
          <Stack
            sx={{ width: "100%", overflow: "hidden" }}
            direction="row"
            justifyContent="space-between"
            alignItems="flex-start"
            spacing={2}
          >
            <Stack
              direction="row"
              divider={
                <Divider
                  orientation="vertical"
                  variant="middle"
                  flexItem
                  sx={{
                    borderColor: "#E6E7EA",
                    marginBlockStart: "12px !important",
                    marginBlockEnd: "12px !important",
                  }}
                />
              }
              sx={{ width: "80%", overflow: "hidden" }}
              spacing={7}
            >
              {[
                {
                  key: "SERVICE",
                  value: props.data.compositeApp,
                },
                {
                  key: "VERSION",
                  value: props.data.compositeAppVersion,
                },
                {
                  key: "CONFIG OVERRIDE",
                  value: props.data.compositeProfile,
                },
              ].map((e) => (
                <Stack key={e.key} sx={{ maxWidth: `25em` }}>
                  <Typography color={"textSecondary"} variant="subtitle2">
                    {e.key}
                  </Typography>
                  <Typography
                    noWrap
                    variant={"h6"}
                    sx={{ fontSize: "1.45rem", fontWeight: "300" }}
                  >
                    {e.value}
                  </Typography>
                </Stack>
              ))}
            </Stack>
            <Button
              style={{ float: "right" }}
              variant="outlined"
              size="small"
              color="primary"
              startIcon={<ListIcon />}
              onClick={() => setOpenActivityLog(true)}
            >
              View Activity Logs
            </Button>
          </Stack>

          {props.data.apps && (
            <>
              <Typography variant={"h6"}>Applications</Typography>
              <Grid
                container
                key="appsContainer"
                spacing={3}
                sx={{
                  marginTop: "0 !important",
                  marginLeft: "-24px !important",
                }}
              >
                {props.data.apps.map((app, appIndex) => (
                  <Grid item xs={12} lg={6} key={app.name + appIndex}>
                    <AppCard
                      app={app}
                      configureAppProps={{
                        setAppToConfigure: setAppToConfigure,
                        onOpenConfigureApp: setOpenConfigureApp,
                      }}
                      appAction={
                        <Button
                          onClick={() =>
                            alert("action button clicked" + app.name)
                          }
                        >
                          Configure
                        </Button>
                      }
                    />
                  </Grid>
                ))}
              </Grid>
            </>
          )}
        </Stack>
      </Box>
    </>
  );
}
export default DeploymentIntentGroupView;
