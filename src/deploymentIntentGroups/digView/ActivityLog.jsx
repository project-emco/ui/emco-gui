//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import React, { useEffect, useState } from "react";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { styled } from "@mui/material/styles";
import {
  Accordion as MuiAccordion,
  AccordionDetails,
  AccordionSummary,
  ListItem,
  ListItemText,
  Typography,
} from "@mui/material";
import List from "@mui/material/List";

const StyledAccordionSummary = styled((props) => (
  <AccordionSummary {...props} expandIcon={<ExpandMoreIcon />} />
))(() => ({
  "&.Mui-expanded": {
    backgroundColor: "#F4F7F8",
  },
}));

export default function ActivityLog({ actions }) {
  const getActivityRows = (actions) => {
    let activityLog = {};
    actions.forEach((action) => {
      let day = new Date(action.time).toLocaleDateString("en-IN", {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
      });
      let time = new Date(action.time).toLocaleString("en-IN", {
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
      });

      let actionLogEntry = { time: time, action: action.state };
      if (activityLog[day]) {
        activityLog[day].actions = [
          ...activityLog[day].actions,
          actionLogEntry,
        ];
      } else {
        let actions = [actionLogEntry];
        activityLog = { [day]: { actions: actions } };
      }
    });
    return activityLog;
  };

  const [activityRows, setActivityRows] = useState();
  useEffect(() => {
    setActivityRows(getActivityRows(actions));
  }, [actions]);

  return (
    <>
      {activityRows &&
        Object.keys(activityRows).map((key) => (
          <MuiAccordion
            disableGutters
            square
            elevation={0}
            key={`accord-${key}`}
          >
            <StyledAccordionSummary>
              <Typography>{key}</Typography>
            </StyledAccordionSummary>
            <AccordionDetails>
              <List>
                <ListItem key="header-item">
                  <ListItemText>Time</ListItemText>
                  <ListItemText>Action</ListItemText>
                </ListItem>
                {activityRows[key].actions.map((item) => (
                  <ListItem key={item.time + item.action}>
                    <ListItemText
                      primaryTypographyProps={{
                        color: "textSecondary",
                        fontWeight: "medium",
                        variant: "body2",
                      }}
                    >
                      {item.time}
                    </ListItemText>
                    <ListItemText
                      primaryTypographyProps={{
                        color: "textSecondary",
                        fontWeight: "medium",
                        variant: "body2",
                      }}
                    >
                      {item.action}
                    </ListItemText>
                  </ListItem>
                ))}
              </List>
            </AccordionDetails>
          </MuiAccordion>
        ))}
    </>
  );
}
