//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================
import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import {
  Divider,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import { ReactComponent as AppPackageIcon } from "../../assets/icons/app_package.svg";
import { ReactComponent as CloudIcon } from "../../assets/icons/cloud_icon.svg";
import Paper from "@mui/material/Paper";
import CheckCircleOutlineRoundedIcon from "@mui/icons-material/CheckCircleRounded";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import DeleteSweepOutlinedIcon from "@mui/icons-material/DeleteSweepOutlined";
import makeStyles from "@mui/styles/makeStyles";

const useStyles = makeStyles(() => ({
  appStatusIcon: {
    width: "20px",
    height: "20px",
    marginRight: "8px",
    marginTop: "1px",
  },
}));

function AppCardTable({ title, headerCellsData, bodyRowsData }) {
  return (
    <TableContainer
      component={Paper}
      sx={{ boxShadow: "none", marginTop: "16px" }}
    >
      <Typography
        sx={{ padding: "11px", fontWeight: "300" }}
        variant="h6"
        id="tableTitle"
        component="div"
      >
        {title}
      </Typography>
      <Table size="small">
        <TableHead>
          <TableRow>
            {headerCellsData.map((cell) => (
              <TableCell key={cell}>{cell}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody
          sx={{
            backgroundColor: "rgb(255,255,255)",
            maxHeight: "100px",
            overflow: "scroll",
          }}
        >
          {bodyRowsData.map((rd) => (
            <TableRow key={rd.id}>
              {rd.columnData &&
                rd.columnData.map((data, index) => (
                  <TableCell key={data + index}>{data.toString()}</TableCell>
                ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

function ClusterAppStatus({ cluster }) {
  const classes = useStyles();
  let status;
  let statusComponent;
  const getStatusIcon = (status) => {
    switch (status) {
      case "Ready":
        return (
          <CheckCircleOutlineRoundedIcon
            className={classes.appStatusIcon}
            color="success"
          />
        );
      case "Deleted":
        return <DeleteSweepOutlinedIcon className={classes.appStatusIcon} />;
      default:
        return <ErrorOutlineIcon className={classes.appStatusIcon} />;
    }
  };

  let ready = true;
  let notPresent = true;
  cluster.resources.every((resource) => {
    if (resource["readyStatus"].toLowerCase() !== "ready") {
      ready = false;
    }
    if (resource["readyStatus"].toLowerCase() !== "notpresent") {
      notPresent = false;
    }
    return ready || notPresent;
  });

  status = ready ? "Ready" : notPresent ? "Deleted" : "Not Ready";
  statusComponent = (
    <Stack direction="row">
      {getStatusIcon(status)}
      <Typography
        component="h2"
        gutterBottom
        color={status === "Ready" ? "green" : undefined}
      >
        {status}
      </Typography>
    </Stack>
  );
  return statusComponent;
}

// function ConfigureApp({ app, configureAppProps }) {
//   const [anchorEl, setAnchorEl] = useState(null);
//   const handleClose = () => {
//     setAnchorEl(null);
//   };
//   const handleMenuOpen = (event) => {
//     setAnchorEl(event.currentTarget);
//   };
//   const handleConfigure = (app) => {
//     handleClose();
//     configureAppProps.setAppToConfigure(app);
//     configureAppProps.onOpenConfigureApp(true);
//   };
//
//   const disableConfigure = (cluster) => {
//     cluster.resources.forEach((resource) => {
//       if (
//         resource.GVK.Kind === "Deployment" &&
//         resource["readyStatus"] === "Applied"
//       ) {
//         return false;
//       }
//     });
//     return true;
//   };
//   return (
//     <>
//       <Button
//         aria-controls={`${app.name}-menu`}
//         aria-haspopup="true"
//         onClick={handleMenuOpen}
//         style={{ float: "right" }}
//         sx={{ color: "#2D3748" }}
//         id={app.name}
//       >
//         <SettingsOutlinedIcon />
//         &nbsp; Configure
//       </Button>
//       <Menu
//         id={`${app.name}-menu`}
//         anchorEl={anchorEl}
//         keepMounted
//         open={Boolean(anchorEl && anchorEl.id && anchorEl.id === app.name)}
//         onClose={handleClose}
//       >
//         {app.clusters.map((cluster) => (
//           <MenuItem
//             style={{ minWidth: "105px" }}
//             key={`${app.name + cluster.cluster}`}
//             disabled={disableConfigure(cluster)}
//             onClick={() => handleConfigure(app)}
//           >
//             {cluster.cluster}
//           </MenuItem>
//         ))}
//       </Menu>
//     </>
//   );
// }

export default function AppCard({ configureAppProps, ...props }) {
  return (
    <Card
      variant="outlined"
      sx={{
        width: "auto",
        background: "#EAEFF180 0% 0% no-repeat padding-box",
        border: "none",
      }}
    >
      <CardContent>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid sx={{ display: "flex", alignItems: "center" }}>
            <AppPackageIcon
              style={{
                fontSize: 30,
                float: "left",
                marginRight: "5px",
              }}
            />
            <Typography variant={"h6"}>{props.app.name}</Typography>
          </Grid>
          {/*<Grid>*/}
          {/*  <ConfigureApp*/}
          {/*    app={props.app}*/}
          {/*    configureAppProps={configureAppProps}*/}
          {/*  />*/}
          {/*</Grid>*/}
        </Grid>
        <Divider
          sx={{
            borderColor: "#E6E7EA",
            marginInlineStart: "-16px",
            marginInlineEnd: "-16px",
            mt: "1rem",
            mb: "1rem",
          }}
        />
        <Stack spacing={3}>
          {props.app.clusters.map((cluster) => (
            <Card
              key={`app-card-${cluster.cluster}-${cluster.clusterProvider}`}
              variant="outlined"
              sx={{
                width: "100%",
                background: "#EAEFF1 0% 0% no-repeat padding-box",
                border: "none",
              }}
            >
              <CardContent>
                <Grid
                  container
                  direction="row"
                  justifyContent="space-between"
                  alignItems="center"
                  spacing={2}
                >
                  <Grid
                    item
                    xs={9}
                    sx={{ display: "flex", alignItems: "center" }}
                  >
                    <Stack sx={{ width: "100%" }} direction="row" spacing={3}>
                      <div style={{ display: "flex" }}>
                        <CloudIcon
                          sx={{
                            fontSize: 30,
                            float: "left",
                            marginRight: "5px",
                          }}
                        />
                        <Stack
                          sx={{
                            width: "80%",
                            maxWidth: "10em",
                            overflow: "hidden",
                          }}
                        >
                          <Typography
                            color={"textSecondary"}
                            sx={{ fontSize: "0.65rem" }}
                            variant="body2"
                          >
                            CLUSTER
                          </Typography>
                          <Typography
                            variant="subtitle1"
                            noWrap
                            sx={{
                              fontWeight: "300",
                              lineHeight: "1.5",
                            }}
                          >
                            {cluster.cluster}
                          </Typography>
                        </Stack>
                      </div>
                      <Stack sx={{ overflow: "hidden" }}>
                        <Typography
                          color={"textSecondary"}
                          sx={{ fontSize: "0.65rem" }}
                          variant="body2"
                        >
                          PROVIDER
                        </Typography>
                        <Typography
                          variant="subtitle1"
                          noWrap
                          sx={{
                            fontWeight: "300",
                            lineHeight: "1.5",
                          }}
                        >
                          {cluster.clusterProvider}
                        </Typography>
                      </Stack>
                    </Stack>
                  </Grid>
                  <Grid item xs={3}>
                    <ClusterAppStatus cluster={cluster} />
                  </Grid>
                </Grid>
                <Stack spacing={2}>
                  {cluster.interfaces && (
                    <AppCardTable
                      title="Interfaces"
                      headerCellsData={["Name", "Network", "Ip Address"]}
                      bodyRowsData={cluster.interfaces.map((ni) => ({
                        id: `th-${ni.spec.interface}-${ni.spec.ipAddress}`,
                        columnData: [
                          ni.spec.interface,
                          ni.spec.name,
                          ni.spec.ipAddress,
                        ],
                      }))}
                    />
                  )}

                  <AppCardTable
                    title="Kubernetes Resources"
                    headerCellsData={["Name", "Kind", "Status"]}
                    bodyRowsData={cluster.resources.map((resource) => ({
                      id: `th-${resource.name}-${resource.GVK.Kind}`,
                      columnData: [
                        resource.name,
                        resource.GVK.Kind,
                        resource.readyStatus,
                      ],
                    }))}
                  />
                </Stack>
              </CardContent>
            </Card>
          ))}
        </Stack>
      </CardContent>
    </Card>
  );
}
