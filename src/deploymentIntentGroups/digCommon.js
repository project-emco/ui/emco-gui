import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import { CircularProgress } from "@mui/material";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import StopScreenShareOutlinedIcon from "@mui/icons-material/StopScreenShareOutlined";

const ServiceInstanceStatus = {
  Created: {
    label: "Not Deployed",
    icon: <RemoveCircleOutlineIcon />,
    color: undefined,
  },
  Deploying: {
    label: "Deploying",
    icon: (
      <CircularProgress
        style={{
          width: "1rem",
          height: "1rem",
          padding: "2px",
        }}
      />
    ),
    color: "primary",
  },
  Deployed: {
    label: "Deployed",
    icon: <CheckCircleOutlineIcon />,
    color: "success",
  },
  DeploymentFailed: {
    label: "Deployment Failed",
    icon: <ErrorOutlineIcon />,
    color: "error",
  },
  Terminating: { label: "Terminating", icon: "", color: "success" },
  Terminated: {
    label: "Terminated",
    icon: <StopScreenShareOutlinedIcon />,
    color: undefined,
  },
  TerminateFailed: {
    label: "Terminate Failed",
    icon: <ErrorOutlineIcon />,
    color: "error",
  },
};

const getDigStatus = (data) => {
  const DeployedStatus = data.deployedStatus;
  const ReadyStatus = data.readyStatus;
  if (!DeployedStatus || DeployedStatus === "") {
    return ServiceInstanceStatus.Created;
  } else {
    if (DeployedStatus === "Instantiating")
      return ServiceInstanceStatus.Deploying;

    if (DeployedStatus === "Terminating")
      return ServiceInstanceStatus.Terminating;

    if (DeployedStatus === "InstantiateFailed")
      return ServiceInstanceStatus.DeploymentFailed;

    if (DeployedStatus === "TerminateFailed")
      return ServiceInstanceStatus.TerminateFailed;

    if (DeployedStatus === "Instantiated") {
      if (ReadyStatus === "Ready") {
        return ServiceInstanceStatus.Deployed;
      } else {
        return ServiceInstanceStatus.Deploying;
      }
    }
    if (DeployedStatus === "Terminated") {
      if (ReadyStatus === "NotPresent") {
        return ServiceInstanceStatus.Terminated;
      } else {
        return ServiceInstanceStatus.TerminateFailed;
      }
    }
  }
};

export { getDigStatus, ServiceInstanceStatus };
