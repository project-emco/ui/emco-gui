//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import React from "react";
import solRestService from "./solRestService";
import {
  IconButton,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import ViewIcon from "@mui/icons-material/Visibility";
import { StyledTable, StyledTableContainer } from "../common/StyledTable";
import { DialogTable } from "../common/DialogTable";
import { SpinnerInContainer } from "../common/Spinner";

const ServiceInstance = () => {
  const [loading, setLoading] = React.useState(true);
  const [serviceInstances, setServiceInstances] = React.useState([]);
  const [showVnfDetails, setShowVnfDetails] = React.useState(false);
  const [rowIndex, setRowIndex] = React.useState(-1);

  const openVnfDetails = (index) => {
    setRowIndex(index);
    setShowVnfDetails(true);
  };

  const vnfDetails = React.useMemo(() => {
    if (rowIndex === -1) return [];
    const currentInstance = serviceInstances[rowIndex];
    return currentInstance.vnfInstance.map((vnf) => {
      return [
        {
          label: "VNF Id",
          id: `${currentInstance.id}-vnf-${vnf.id}-id`,
          content: vnf.id,
        },
        {
          label: "VNFD Id",
          id: `${currentInstance.id}-vnfd-${vnf.id}-id`,
          content: vnf.vnfdId,
        },
        {
          label: "State",
          id: `${currentInstance.id}-vnf-${vnf.id}-state`,
          content: vnf.instantiationState,
        },
      ];
    });
  }, [rowIndex, serviceInstances]);

  React.useEffect(() => {
    const fetchServiceInstances = async () => {
      setLoading(true);
      const serviceInstances = await solRestService.getServiceInstances();
      setServiceInstances(serviceInstances);
      setLoading(false);
    };
    fetchServiceInstances();
  }, []);

  return loading ? (
    <SpinnerInContainer />
  ) : (
    <>
      <StyledTableContainer>
        <StyledTable size="small">
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>NSD ID</TableCell>
              <TableCell sx={{ textAlign: "center" }}>VNF</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {serviceInstances.map((instance, index) => (
              <TableRow key={instance.id}>
                <TableCell>{instance.id}</TableCell>
                <TableCell>{instance.nsInstanceName}</TableCell>
                <TableCell>{instance.nsInstanceDescription}</TableCell>
                <TableCell>{instance.nsState}</TableCell>
                <TableCell>{instance.nsdInfoId}</TableCell>
                <TableCell sx={{ textAlign: "center" }}>
                  <IconButton
                    aria-haspopup="true"
                    aria-controls="vnf-details"
                    title="Show VNF details"
                    size="small"
                    onClick={() => openVnfDetails(index)}
                  >
                    <ViewIcon color="primary" />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </StyledTable>
      </StyledTableContainer>
      <DialogTable
        label="VNF Details"
        open={showVnfDetails}
        onClose={() => setShowVnfDetails(false)}
        rows={vnfDetails}
      />
    </>
  );
};

export default ServiceInstance;
