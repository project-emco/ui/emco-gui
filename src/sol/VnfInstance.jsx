//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import React from "react";
import solRestService from "./solRestService";
import {
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import { SpinnerInContainer } from "../common/Spinner";
import { StyledTable, StyledTableContainer } from "../common/StyledTable";

const VnfInstance = ({ projectName }) => {
  const [loading, setLoading] = React.useState(true);
  const [resources, setResources] = React.useState([]);

  React.useEffect(() => {
    if (!projectName) return;
    const fetchResources = async () => {
      setLoading(true);
      setResources(await solRestService.getVnfInstances(projectName));
      setLoading(false);
    };
    fetchResources();
  }, [projectName]);

  return loading ? (<SpinnerInContainer />) : (
    <StyledTableContainer>
      <StyledTable>
        <TableHead>
          <TableRow>
            <TableCell>Id</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Description</TableCell>
            <TableCell>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {resources.map((resource, index) => (
            <TableRow key={index}>
              <TableCell>{resource.spec.id}</TableCell>
              <TableCell>{resource.metadata.name}</TableCell>
              <TableCell>{resource.metadata.description}</TableCell>
              <TableCell>{resource.spec.action}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </StyledTable>
    </StyledTableContainer>
  );
};

export default VnfInstance;
