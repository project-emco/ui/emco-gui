//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import { requestRest } from "../services/apiService";

class SOLRestService {
  async getVnfDescriptors(projectName) {
    const response = await requestRest({
      url: `/v2/projects/${projectName}/composite-apps`,
    });
    return response.data;
  }

  async getVnfInstances(projectName) {
    const response = await requestRest({
      url: `/middleend/projects/${projectName}/deployment-intent-groups`,
    });
    return response.data;
  }

  async getServiceDescriptors() {
    const response = await requestRest({
      url: `/api/etsi/nslcm/v3/nsd`,
    });
    return response.data;
  }

  async getServiceInstances() {
    const response = await requestRest({
      url: `/api/etsi/nslcm/v3/ns_instances`,
    });
    return response.data;
  }
}

export const solRestService = new SOLRestService();
export default solRestService;
