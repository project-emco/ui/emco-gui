//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import {
  Card,
  CardContent,
  CardMedia,
  Grid,
  Typography,
  styled,
} from "@mui/material";
import React from "react";
import { tenantMenu } from "../config/uiConfig";
import { Link, useLocation } from "react-router-dom";

const StyledLink = styled(Link)`
  display: flex;
  text-decoration: none;
  height: 100%;
  color: ${({ theme }) => theme.palette.primary.main};

  &:hover {
    color: ${({ theme }) => theme.palette.primary.light};
  }
`;

const StyledCard = styled(Card)`
  display: flex;
  flex-direction: column;
  width: 100%;
  text-align: center;
  color: inherit;
`;

const StyledCardMedia = styled(CardMedia)`
  padding: ${({ theme }) => theme.spacing(3)};

  svg {
    font-size: 2rem;
  }
`;

const StyledCardContent = styled(CardContent)`
  margin-top: auto;
`;

const solMenu = tenantMenu?.[0]?.children?.find((item) => item.id === "SOL");

const getLinkUrl = (basePath, url) => {
  return url ? basePath.replace(/\/sol$/, "") + url : basePath;
}

const SolTabs = () => {
  const location = useLocation();

  if (!solMenu) return null;

  return (
    <Grid container spacing={2}>
      {solMenu.children.map((child) => (
        <Grid item key={child.id} xs={12} sm={6} md={3}>
          <StyledLink to={getLinkUrl(location.pathname, child.url)}>
            <StyledCard>
              <StyledCardMedia>{child.icon}</StyledCardMedia>
              <StyledCardContent>
                <Typography
                  variant="body2"
                  component="div"
                  textTransform="uppercase"
                >
                  {child.id}
                </Typography>
              </StyledCardContent>
            </StyledCard>
          </StyledLink>
        </Grid>
      ))}
    </Grid>
  );
};

export default SolTabs;
