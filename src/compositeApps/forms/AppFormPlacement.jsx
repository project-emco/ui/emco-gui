import React from "react";
import PropTypes from "prop-types";
import { Grid, Paper, Typography } from "@mui/material";
import ClustersTable from "./DIGPlacementTable";
import LabelsTable from "./DIGPlacementTableLabels";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";

function RadioButtonsGroup({ formikProps, index }) {
  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">Criterion</FormLabel>
      <RadioGroup
        row
        aria-label="criterion"
        name={`apps[${index}].placementCriterion`}
        value={formikProps.values.apps[index].placementCriterion}
        onChange={formikProps.handleChange}
        onBlur={formikProps.handleBlur}
      >
        <FormControlLabel value="allOf" control={<Radio />} label="All Of" />
        <FormControlLabel value="anyOf" control={<Radio />} label="Any Of" />
      </RadioGroup>
      <FormHelperText>Criterion for the app placement.</FormHelperText>
    </FormControl>
  );
}

function CustomSelect({ formikProps, index }) {
  return (
    <div>
      <FormControl>
        <InputLabel id="app-placement-type">Type</InputLabel>
        <Select
          labelId="app-placement-type-helper-label"
          name={`apps[${index}].placementType`}
          value={formikProps.values.apps[index].placementType}
          onChange={formikProps.handleChange}
          onBlur={formikProps.handleBlur}
        >
          <MenuItem value="labels">Labels</MenuItem>
          <MenuItem value="clusters">Specific Clusters</MenuItem>
        </Select>
        <FormHelperText style={{ marginTop: "12px" }}>
          {`Select targets based on the ${formikProps.values.apps[index].placementType}.`}
        </FormHelperText>
      </FormControl>
    </div>
  );
}

function AppFormPlacement({
  formikProps,
  index,
  logicalCloud,
  handleRowSelect,
}) {
  return (
    <>
      <Typography variant="subtitle1" style={{ float: "left" }}>
        Select Clusters
        <span className="MuiFormLabel-asterisk MuiInputLabel-asterisk"> *</span>
      </Typography>
      {formikProps.errors.apps &&
        formikProps.errors.apps[index] &&
        formikProps.errors.apps[index].clusters && (
          <span
            style={{
              color: "#f44336",
              marginRight: "35px",
              float: "right",
            }}
          >
            {typeof formikProps.errors.apps[index].clusters === "string" &&
              formikProps.errors.apps[index].clusters}
          </span>
        )}
      <Grid
        container
        spacing={3}
        style={{
          height: "400px",
          overflowY: "auto",
          width: "100%",
        }}
      >
        <Grid key={"placementTypeGrid"} item xs={6}>
          <Paper style={{ padding: "14px 10px" }}>
            <CustomSelect formikProps={formikProps} index={index} />
          </Paper>
        </Grid>
        <Grid key={"placementCriterionGrid"} item xs={6}>
          <Paper style={{ padding: "14px 10px" }}>
            <RadioButtonsGroup formikProps={formikProps} index={index} />
          </Paper>
        </Grid>
        {logicalCloud &&
          logicalCloud.spec.clusterReferences.spec.clusterProviders.length >
            0 &&
          logicalCloud.spec.clusterReferences.spec.clusterProviders.map(
            (clusterProvider) => (
              <Grid key={clusterProvider.metadata.name} item xs={12}>
                <Paper>
                  {formikProps.values.apps[index].placementType ===
                  "clusters" ? (
                    <ClustersTable
                      key={clusterProvider.metadata.name}
                      tableName={clusterProvider.metadata.name}
                      clusters={clusterProvider.spec.clusters}
                      formikValues={formikProps.values.apps[index].clusters}
                      onRowSelect={handleRowSelect}
                    />
                  ) : (
                    <LabelsTable
                      key={clusterProvider.metadata.name}
                      tableName={clusterProvider.metadata.name}
                      labels={clusterProvider.spec.labels}
                      formikValues={formikProps.values.apps[index].clusters}
                      onRowSelect={handleRowSelect}
                    />
                  )}
                </Paper>
              </Grid>
            )
          )}
      </Grid>
    </>
  );
}

AppFormPlacement.propTypes = {
  formikProps: PropTypes.object,
  index: PropTypes.number,
  handleRowSelect: PropTypes.func,
};

export default AppFormPlacement;
