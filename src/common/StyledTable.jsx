//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import React from "react";
import { Paper, Table, TableContainer, styled } from "@mui/material";

const StyledComponents = {
  Table: styled(Table)`
    thead * {
      text-transform: uppercase;
    }
    tbody tr:last-of-type td {
      border: none;
    }
    tbody > tr:nth-of-type(odd) {
      background-color: ${({ theme }) => theme.palette.action.hover};
    }
  `,
};

export const StyledTableContainer = (props) => (
  <TableContainer component={Paper} {...props} />
);

export const StyledTable = (props) => <StyledComponents.Table {...props} />;
