import React, { useState } from "react";
import makeStyles from '@mui/styles/makeStyles';
import clsx from "clsx";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import StorageIcon from "@mui/icons-material/Storage";
import ErrorIcon from "@mui/icons-material/Error";
import DeleteIcon from "@mui/icons-material/DeleteTwoTone";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
}));
const ExpandableCard = (props) => {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(
    props.expanded ? props.expanded : false
  );

  const handleExpandClick = () => {
    if (!expanded) {
      setExpanded(!expanded);
    } else {
      setExpanded(!expanded);
    }
  };

  return <>
    <Card className={classes.root}>
      <CardHeader
        onClick={handleExpandClick}
        avatar={
          <>
            <StorageIcon fontSize="large" />
          </>
        }
        action={
          <>
            {props.appIndex !== undefined && (
              <IconButton
                color="primary"
                onClick={(e) => {
                  e.stopPropagation();
                  props.handleRemoveApp(props.appIndex);
                }}
                size="large">
                <DeleteIcon />
              </IconButton>
            )}
            {props.error && (
              <ErrorIcon color="error" style={{ verticalAlign: "middle" }} />
            )}
            <IconButton
              className={clsx(classes.expand, {
                [classes.expandOpen]: expanded,
              })}
              onClick={handleExpandClick}
              aria-expanded={expanded}
              size="large">
              <ExpandMoreIcon />
            </IconButton>
          </>
        }
        title={props.title}
        subheader={props.description}
      />
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>{props.content}</CardContent>
      </Collapse>
    </Card>
  </>;
};

ExpandableCard.propTypes = {};

export default ExpandableCard;
