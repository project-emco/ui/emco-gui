//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import React from "react";
import {
  AppBar,
  Box,
  Card,
  Dialog,
  IconButton,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Toolbar,
  Typography,
  styled,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { StyledTable, StyledTableContainer } from "./StyledTable";

const StyledAppBar = styled(AppBar)`
  position: sticky;
  box-shadow: none;
`;

const StyledToolbar = styled(Toolbar)`
  justify-content: space-between;
`;

/**
 * @typedef {Object} TableCell
 * @property {string} id - the unique identifier for the cell
 * @property {string} label - the label to be displayed for the cell
 * @property {(string|JSX.Element)} content - the content to be displayed in the cell
 */

/**
 * @typedef {TableCell[]} TableRow
 */

/**
 * @typedef {TableRow[]} RowsArray
 */

/**
 * @typedef {Object} DialogTableProps
 * @property {string} label - the label for the dialog
 * @property {boolean} open - whether the dialog is open or not
 * @property {() => void} onClose - callback function to close the dialog
 * @property {RowsArray} rows - the rows to be displayed in the table
 */

/**
 * @function DialogTable
 * @param {DialogTableProps} props - the component props
 * @returns {JSX.Element}
 */
export const DialogTable = ({ label, open, onClose, rows }) => {
  const Header = React.useMemo(() => {
    if (!rows?.length) return null;
    return (
      <TableHead>
        <TableRow>
          {rows[0].map((cell) => (
            <TableCell key={cell.id}>{cell.label}</TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }, [rows]);

  const Body = React.useMemo(() => {
    if (!rows?.length) return null;
    return (
      <TableBody>
        {rows.map((row, index) => (
          <TableRow key={index}>
            {row.map((cell) => (
              <TableCell key={cell.id}>
                {cell.content}
              </TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    );
  }, [rows]);

  return (
    <Dialog maxWidth={"md"} open={open} onClose={onClose}>
      <StyledAppBar>
        <StyledToolbar>
          <Typography component="h2" variant="h6">
            {label}
          </Typography>
          <IconButton
            edge="end"
            color="inherit"
            onClick={onClose}
            aria-label="close"
            size="large"
          >
            <CloseIcon />
          </IconButton>
        </StyledToolbar>
      </StyledAppBar>
      <Box m={2}>
        <Card variant="outlined" style={{ marginBottom: 4 }}>
          <StyledTableContainer>
            <StyledTable>
              {Header}
              {Body}
            </StyledTable>
          </StyledTableContainer>
        </Card>
      </Box>
    </Dialog>
  );
};
