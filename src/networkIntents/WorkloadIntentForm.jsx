//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================  
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import MuiDialogTitle from '@mui/material/DialogTitle';
import MuiDialogContent from '@mui/material/DialogContent';
import MuiDialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import { TextField, Select, MenuItem, FormControl, InputLabel, FormHelperText, Grid } from '@mui/material';
import * as Yup from "yup";
import { Formik } from 'formik';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton className={classes.closeButton} onClick={onClose} size="large">
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    }
}))(MuiDialogContent);

const schema = Yup.object(
    {
        name: Yup.string().required(),
        description: Yup.string(),
        appName: Yup.string().required(),
        workloadResource: Yup.string().required(),
        type: Yup.string().required(),
    })
const WorkloadIntentForm = (props) => {
    const { onClose, item, open, onSubmit, appsData } = props;
    const buttonLabel = item ? "OK" : "Add"
    const title = item ? "Edit Workload Intent" : "Add Workload Intent"
    const handleClose = () => {
        onClose();
    };
    let initialValues = item ?
        { name: item.metadata.name, description: item.metadata.description, appName: item.spec["app-name"] } :
        { name: "", description: "", appName: appsData[0].metadata.name, workloadResource: "", type: "" }

    return (
        <Dialog
            maxWidth={"xs"}
            onClose={handleClose}
            aria-labelledby="customized-dialog-title"
            open={open}>
            <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
            <Formik
                initialValues={initialValues}
                onSubmit={async values => {
                    onSubmit(values);
                }}
                validationSchema={schema}
            >
                {props => {
                    const {
                        values,
                        touched,
                        errors,
                        isSubmitting,
                        handleChange,
                        handleBlur,
                        handleSubmit
                    } = props;
                    return (
                        <form encType="multipart/form-data" noValidate onSubmit={handleSubmit}>
                            <DialogContent dividers>
                                <Grid container spacing={3}>
                                    <Grid item xs={6}>
                                        <TextField
                                            style={{ float: "left", marginBottom: "10px" }}
                                            id="name"
                                            label="Name"
                                            type="text"
                                            value={values.name}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            helperText={(errors.name && touched.name && (
                                                "Name is required"
                                            ))}
                                            required
                                            error={errors.name && touched.name}
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <FormControl error={errors.appName && touched.appName}>
                                            <InputLabel id="appname-select-label">App</InputLabel>
                                            <Select
                                                labelId="appname-select-label"
                                                id="appname-select-label"
                                                name="appName"
                                                value={values.appName}
                                                onChange={handleChange}>
                                                {appsData.map(app =>
                                                    (<MenuItem key={app.metadata.name} value={app.metadata.name}>{app.metadata.name}</MenuItem>)
                                                )}
                                            </Select>
                                            {errors.appName && touched.appName && <FormHelperText>Required</FormHelperText>}
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField
                                            id="workloadResource"
                                            label="Workload Resource"
                                            type="text"
                                            name="workloadResource"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            helperText={(errors.workloadResource && touched.workloadResource && (
                                                "Workload Resource is required"
                                            ))}
                                            required
                                            error={errors.workloadResource && touched.workloadResource}
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField
                                            id="type"
                                            label="Type"
                                            type="text"
                                            name="type"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            helperText={(errors.type && touched.type && (
                                                "Type is required"
                                            ))}
                                            required
                                            error={errors.type && touched.type}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField
                                            style={{ width: "100%" }}
                                            name="description"
                                            value={values.description}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            id="description"
                                            label="Description"
                                            multiline
                                            maxRows={4}
                                        />
                                    </Grid>
                                </Grid>
                            </DialogContent>
                            <DialogActions>
                                <Button autoFocus onClick={handleClose} color="secondary">
                                    Cancel
                                </Button>
                                <Button autoFocus type="submit" color="primary" disabled={isSubmitting}>
                                    {buttonLabel}
                                </Button>
                            </DialogActions>
                        </form>
                    );
                }}
            </Formik>
        </Dialog>
    );
};

WorkloadIntentForm.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    item: PropTypes.object,
    onSubmit: PropTypes.func.isRequired,
    appsData: PropTypes.arrayOf(PropTypes.object).isRequired
};
export default WorkloadIntentForm;
