//=======================================================================
// Copyright (c) 2017-2020 Aarna Networks, Inc.
// All rights reserved.
// ======================================================================
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//           http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ========================================================================

import React from "react";
import DashboardIcon from "@mui/icons-material/Dashboard";
import DeviceHubIcon from "@mui/icons-material/DeviceHub";
import DnsRoundedIcon from "@mui/icons-material/DnsRounded";
import PeopleIcon from "@mui/icons-material/People";
import SettingsIcon from "@mui/icons-material/SettingsRounded";
import LogicalCloudIcon from "@mui/icons-material/SettingsSystemDaydream";
import CloudSyncIcon from "@mui/icons-material/Input";
import BusinessIcon from "@mui/icons-material/Business";
import LanIcon from "@mui/icons-material/Lan";
import HubIcon from "@mui/icons-material/Hub";
import SettingsEthernetIcon from "@mui/icons-material/SettingsEthernet";
import Diversity2Icon from "@mui/icons-material/Diversity2";
import DisplaySettingsIcon from "@mui/icons-material/DisplaySettings";
import StreamIcon from "@mui/icons-material/Stream";

const windowEnv = window._env_ || {};

let { ENABLE_RBAC, ENABLE_SMO, ENABLE_SOL } = windowEnv;

const SMO_ENABLED = ENABLE_SMO === "true";
const RBAC_ENABLED = ENABLE_RBAC === "true";
const SOL_ENABLED = ENABLE_SOL === "true";

const adminMenu = [
  {
    id: "adminMenu",
    children: [
      {
        id: "Tenants",
        icon: <BusinessIcon />,
        url: "/projects",
      },
      {
        id: "K8s Controllers",
        icon: <SettingsIcon />,
        url: "/controllers",
      },
      {
        id: "Clusters",
        icon: <DnsRoundedIcon />,
        url: "/clusters",
      },
    ],
  },
];

if (RBAC_ENABLED) {
  adminMenu[0].children.push({
    id: "Users",
    icon: <PeopleIcon />,
    url: "/users",
  });
}

if (SMO_ENABLED) {
  adminMenu[0].children.push({
    id: "SMO",
    icon: <CloudSyncIcon />,
    url: "/smo",
  });
}

const tenantMenu = [
  {
    id: "tenantMenu",
    children: [
      {
        id: "Dashboard",
        icon: <DashboardIcon />,
        url: "/dashboard",
      },
      {
        id: "Services",
        icon: <DeviceHubIcon />,
        url: "/services",
      },
      {
        id: "Logical Clouds",
        icon: <LogicalCloudIcon />,
        url: "/logical-clouds",
      },
      {
        id: "Service Instances",
        icon: <DnsRoundedIcon />,
        url: "/deployment-intent-groups",
      },
      {
        id: "Service Instance Group",
        icon: <LanIcon />,
        url: "/service-instance-group",
      },
    ],
  },
];

if (SOL_ENABLED) {
  tenantMenu[0].children.push({
    id: "SOL",
    icon: <HubIcon />,
    url: "/sol",
    children: [
      {
        id: "VNF Descriptor",
        icon: <SettingsEthernetIcon />,
        url: "/sol/vnf-descriptor",
      },
      {
        id: "VNF Instance",
        icon: <Diversity2Icon />,
        url: "/sol/vnf-instance",
      },
      {
        id: "Service Descriptor",
        icon: <DisplaySettingsIcon />,
        url: "/sol/service-descriptor",
      },
      {
        id: "Service Instance",
        icon: <StreamIcon />,
        url: "/sol/service-instance",
      },
    ],
  });
}

const routes = [
  { path: "/app/admin/projects", name: "Tenants" },
  { path: "/app/admin/clusters", name: "Clusters" },
  { path: "/app/admin/controllers", name: "Controllers" },
  { path: "/app/admin/smo", name: "SMO" },
  { path: "/app/admin/users", name: "Users" },
  { path: "/app/projects/:projectName/Dashboard", name: "Dashboard" },
  { path: "/app/projects/:projectName/services", name: "Services" },
  { path: "/app/projects/:projectName/logical-clouds", name: "Logical Clouds" },
  {
    path: "/app/projects/:projectName/service",
    name: "Service",
  },
  {
    path: "/app/projects/:projectName/deployment-intent-groups",
    name: "Service Instances",
  },
  {
    path: "/app/projects/:projectName/service-instance-group",
    name: "Service Instance Group",
  },
  {
    path: "/app/projects/:projectName/deployment-intent-groups/:compositeAppName/:compositeAppVersion/:digName/status",
    name: "Service Instance Detail",
  },
  {
    path: "/app/projects/:projectName/deployment-intent-groups/:compositeAppName/:compositeAppVersion/:digName/checkout",
    name: "Service Instance Detail",
  },
  {
    path: "/app/projects/:projectName/services/:appname/:version",
    param: "appname",
  },
  {
    path: "/app/projects/:projectName/sol/vnf-descriptor",
    name: "VNF Descriptor",
  },
  {
    path: "/app/projects/:projectName/sol/vnf-instance",
    name: "VNF Instance",
  },
  {
    path: "/app/projects/:projectName/sol/service-descriptor",
    name: "Service Descriptor",
  },
  {
    path: "/app/projects/:projectName/sol/service-instance",
    name: "Service Instance",
  },
  {
    path: "/app/projects/:projectName/sol",
    name: "SOL",
  },
];

export {
  adminMenu,
  tenantMenu,
  SMO_ENABLED,
  SOL_ENABLED,
  RBAC_ENABLED,
  routes,
};

const defaultExports = {
  adminMenu,
  tenantMenu,
  SMO_ENABLED,
  SOL_ENABLED,
  RBAC_ENABLED,
};

export default defaultExports;
