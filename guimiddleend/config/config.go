package config

import (
	"encoding/json"
)

const CfgFilePath = "/opt/emco/config/middleend.conf"

// MiddleendConfig represents the configmap of Middleend
type MiddleendConfig struct {
	OwnPort        string `json:"ownport"`
	Clm            string `json:"clm"`
	Dcm            string `json:"dcm"`
	Ncm            string `json:"ncm"`
	Gac            string `json:"gac"`
	Dtc            string `json:"dtc"`
	Its            string `json:"its"`
	OrchService    string `json:"orchestrator"`
	OvnService     string `json:"ovnaction"`
	CfgService     string `json:"configSvc"`
	Mongo          string `json:"mongo"`
	LogLevel       string `json:"logLevel"`
	ZipkinIp       string `json:"zipkin-ip"`
	ZipkinPort     string `json:"zipkin-port"`
	JaegerIp       string `json:"jaeger-ip"`
	JaegerPort     string `json:"jaeger-port"`
	AppInstantiate bool   `json:"appInstantiate"`
	StoreName      string `json:"storeName"`
}

func Parse(byteValue []byte) (*MiddleendConfig, error) {
	cfg := &MiddleendConfig{}
	err := json.Unmarshal(byteValue, cfg)
	return cfg, err
}
