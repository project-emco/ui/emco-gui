package app

import (
	"net/http"

	"example.com/middleend/config"
	"github.com/gorilla/mux"
)

type HandleFunc func(string, func(http.ResponseWriter, *http.Request)) *mux.Route

const (
	cAppUriPattern = "/projects/{projectName}/composite-apps/{compositeAppName}"
	digUriPattern  = cAppUriPattern + "/{version}/deployment-intent-groups/{deploymentIntentGroupName}"
)

// RegisterHandlers is a helper function for net/http/HandleFunc
// This function was introduced as a workaround for a concurrency issue in the middleend code
// The receiver for handlers functions was not thread-safe.
// All ServeHTTP functions required a refactoring to instantiate a new instance of
// the receiver, OrchestrationHandler, on every request.
// Ideally, future endpoints should not be using the closure for creating instance,
// instead do it as part handler intialization
func RegisterHandlers(handle HandleFunc, bootConf config.MiddleendConfig) {
	createInstance := func() *OrchestrationHandler {
		o := NewAppHandler()
		o.MiddleendConf = bootConf
		return o
	}
	handle("/healthcheck", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetHealth(w)
	}).Methods("GET")
	// APIs related to service checkout
	handle(cAppUriPattern+"/{version}/checkout", func(w http.ResponseWriter, r *http.Request) {
		createInstance().CreateDraftCompositeApp(w, r)
	}).Methods("POST")

	handle(cAppUriPattern+"/versions", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetSvcVersions(w, r)
	}).Methods("GET")

	handle(cAppUriPattern+"/versions/", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetSvcVersions(w, r)
	}).Queries("state", "{state}")

	handle(cAppUriPattern+"/{version}/app", func(w http.ResponseWriter, r *http.Request) {
		createInstance().UpdateCompositeApp(w, r)
	}).Methods("POST")
	handle(cAppUriPattern+"/{version}/apps/{appName}", func(w http.ResponseWriter, r *http.Request) {
		createInstance().RemoveApp(w, r)
	}).Methods("DELETE")

	handle(cAppUriPattern+"/{version}/update", func(w http.ResponseWriter, r *http.Request) {
		createInstance().CreateService(w, r)
	}).Methods("POST")

	// POST, GET, DELETE composite apps
	handle("/projects/{projectName}/composite-apps", func(w http.ResponseWriter, r *http.Request) {
		createInstance().CreateApp(w, r)
	}).Methods("POST")

	handle(cAppUriPattern+"/{version}", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetSvc(w, r)
	}).Methods("GET")

	handle("/projects/{projectName}/composite-apps", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetSvc(w, r)
	}).Methods("GET")

	handle("/projects/{projectName}/composite-apps", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetSvc(w, r)
	}).Queries("filter", "{filter}")

	handle(cAppUriPattern+"/{version}", func(w http.ResponseWriter, r *http.Request) {
		createInstance().DelSvc(w, r)
	}).Methods("DELETE")

	// POST, GET, DELETE deployment intent groups
	handle(cAppUriPattern+"/{version}/deployment-intent-groups", func(w http.ResponseWriter, r *http.Request) {
		createInstance().CreateDig(w, r)
	}).Methods("POST")

	handle("/projects/{projectName}/deployment-intent-groups", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetAllDigs(w, r)
	}).Methods("GET")

	handle(cAppUriPattern+"/{version}/deployment-intent-groups", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetAllDigs(w, r)
	}).Methods("GET")

	handle(digUriPattern, func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetAllDigs(w, r)
	}).Methods("GET")

	handle(digUriPattern, func(w http.ResponseWriter, r *http.Request) {
		createInstance().DelDig(w, r)
	}).Methods("DELETE")

	handle(digUriPattern+"/", func(w http.ResponseWriter, r *http.Request) {
		createInstance().DelDig(w, r)
	}).Queries("operation", "{operation}").Methods("DELETE")

	handle(digUriPattern+"/status", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetDigStatus(w, r)
	}).Methods("GET")

	// DIG migrate/update/rollback related APIs
	handle(digUriPattern+"/checkout", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetDigInEdit(w, r)
	}).Methods("GET")

	handle(digUriPattern+"/checkout", func(w http.ResponseWriter, r *http.Request) {
		createInstance().CheckoutDIG(w, r)
	}).Methods("POST")

	handle(digUriPattern+"/checkout/", func(w http.ResponseWriter, r *http.Request) {
		createInstance().CheckoutDIG(w, r)
	}).Queries("operation", "{operation}").Methods("POST")

	handle(digUriPattern+"/checkout/submit", func(w http.ResponseWriter, r *http.Request) {
		createInstance().UpgradeDIG(w, r)
	}).Methods("POST")

	handle(digUriPattern+"/checkout", func(w http.ResponseWriter, r *http.Request) {
		createInstance().DigUpdateHandler(w, r)
	}).Methods("PUT")

	handle(digUriPattern+"/checkout/", func(w http.ResponseWriter, r *http.Request) {
		createInstance().DigUpdateHandler(w, r)
	}).Queries("operation", "{operation}").Methods("PUT")

	handle(digUriPattern+"/scaleout", func(w http.ResponseWriter, r *http.Request) {
		createInstance().ScaleOutDig(w, r)
	}).Methods("POST")

	// GAC related APIs
	handle(digUriPattern+"/resources", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetK8sResources(w, r)
	}).Methods("GET")

	handle(digUriPattern+"/resources/{resourceName}", func(w http.ResponseWriter, r *http.Request) {
		createInstance().DeleteK8sResources(w, r)
	}).Methods("DELETE")

	handle(digUriPattern+"/resources/{resourceName}/customizations/{customizationName}",
		func(w http.ResponseWriter, r *http.Request) {
			createInstance().DeleteK8sResourceCustomizations(w, r)
		}).Methods("DELETE")
	// ClusterProvider/Cluster creation APIs
	handle("/cluster-providers", func(w http.ResponseWriter, r *http.Request) {
		createInstance().CreateClusterProvider(w, r)
	}).Methods("POST")
	handle("/cluster-providers/{clusterProvider}", func(w http.ResponseWriter, r *http.Request) {
		createInstance().DeleteClusterProvider(w, r)
	}).Methods("DELETE")
	handle("/cluster-providers/{cluster-provider-name}/clusters", func(w http.ResponseWriter, r *http.Request) {
		createInstance().OnboardCluster(w, r)
	}).Methods("POST")
	handle("/cluster-providers/{clusterProvider}/clusters/{cluster}", func(w http.ResponseWriter, r *http.Request) {
		createInstance().DeleteCluster(w, r)
	}).Methods(http.MethodDelete)
	handle("/all-clusters", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetClusters(w, r)
	}).Methods("GET")

	//GET dashboard
	handle("/projects/{projectName}/dashboard", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetDashboardData(w, r)
	}).Methods("GET")

	// Logical Cloud related APIs
	handle("/projects/{projectName}/logical-clouds", func(w http.ResponseWriter, r *http.Request) {
		createInstance().HandleLCCreateRequest(w, r)
	}).Methods("POST")
	handle("/projects/{projectName}/logical-clouds", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetLogicalClouds(w, r)
	}).Methods("GET")
	handle("/projects/{projectName}/logical-clouds/{logicalCloud}", func(w http.ResponseWriter, r *http.Request) {
		createInstance().DeleteLogicalCloud(w, r)
	}).Methods("DELETE")
	handle("/projects/{projectName}/logical-clouds/{logicalCloud}", func(w http.ResponseWriter, r *http.Request) {
		createInstance().UpdateLogicalCloud(w, r)
	}).Methods("PUT")
	handle("/projects/{projectName}/logical-cloud/{logicalCloud}/status", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetLogicalCloudsStatus(w, r)
	}).Methods("GET")

	// Get cluster networks
	handle("/cluster-providers/{clusterprovider-name}/clusters/{cluster-name}/networks",
		func(w http.ResponseWriter, r *http.Request) {
			createInstance().GetClusterNetworks(w, r)
		}).Methods("GET")

	handle("/projects/{projectName}/service-instance-groups", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetAllServiceInstanceGroups(w, r)
	}).Methods("GET")

	handle("/projects/{projectName}/service-instance-groups/{serviceInstanceGroupName}", func(w http.ResponseWriter, r *http.Request) {
		createInstance().GetServiceInstanceGroup(w, r)
	}).Methods("GET")
}
