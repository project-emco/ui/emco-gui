package app

import (
	"context"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type ServiceInstanceGroupHandler struct {
	orchInstance *OrchestrationHandler
	orchURL      string
}

type Service struct {
	Metadata ServiceInstanceGroupMetadata `json:"metadata,omitempty"`
	Spec     ServiceInstanceGroupSpec     `json:"spec,omitempty"`
	Status   ServiceInstanceGroupStatus   `json:"status,omitempty"`
}

type ServiceInstanceGroupMetadata struct {
	Name        string `json:"name"`
	Id          string `json:"id"`
	Project     string `json:"project"`
	Description string `json:"description"`
}

// ServiceInstanceGroupSpec Spec info of the service
type ServiceInstanceGroupSpec struct {
	Digs []string `json:"digs"`
}

// ServiceInstanceGroupStatus status of the service
type ServiceInstanceGroupStatus struct {
	DeployedStatus string `json:"deployedStatus"`
	ReadyStatus    string `json:"readyStatus"`
}

func (h *ServiceInstanceGroupHandler) GetServiceInstanceGroups(ctx context.Context) (interface{}, interface{}, interface{}) {
	orch := h.orchInstance
	vars := orch.Vars
	h.orchURL = "http://" + orch.MiddleendConf.OrchService + "/v2/projects/" +
		vars["projectName"] + "/services"
	log.Infof("services object URL: %s", h.orchURL)
	//proj1
	respcode, respdata, err := orch.apiGet(ctx, h.orchURL, vars["projectName"]+"_services")
	if respcode != http.StatusOK {
		return respcode, nil, nil
	}
	var serviceJsonArrayResponse []Service
	err = json.Unmarshal(respdata, &serviceJsonArrayResponse)
	if err != nil {
		panic(err)
	}
	for index, service := range serviceJsonArrayResponse {
		serviceJsonArrayResponse[index].Status, err = h.getServiceInstanceGroupStatus(ctx, service.Metadata.Name)
	}
	return respcode, serviceJsonArrayResponse, err
}

func (h *ServiceInstanceGroupHandler) GetServiceInstanceGroup(ctx context.Context) (interface{}, interface{}, interface{}) {
	orch := h.orchInstance
	vars := orch.Vars
	h.orchURL = "http://" + orch.MiddleendConf.OrchService + "/v2/projects/" +
		vars["projectName"] + "/services/" + vars["serviceInstanceGroupName"]
	log.Infof("service object URL: %s", h.orchURL)
	//proj1
	respcode, respdata, err := orch.apiGet(ctx, h.orchURL, vars["serviceInstanceGroups"]+"_serviceInstanceGroup")
	if respcode != http.StatusOK {
		return respcode, nil, nil
	}
	var serviceJsonResponse Service
	err = json.Unmarshal(respdata, &serviceJsonResponse)
	if err != nil {
		panic(err)
	}
	serviceJsonResponse.Status, err = h.getServiceInstanceGroupStatus(ctx, vars["serviceInstanceGroupName"])
	return respcode, serviceJsonResponse, err
}

func (h *ServiceInstanceGroupHandler) getServiceInstanceGroupStatus(ctx context.Context, serviceInstanceGroupName string) (ServiceInstanceGroupStatus, error) {
	orch := h.orchInstance
	vars := orch.Vars
	h.orchURL = "http://" + orch.MiddleendConf.OrchService + "/v2/projects/" +
		vars["projectName"] + "/services/" + serviceInstanceGroupName + "/status"
	log.Infof("composite app object URL: %s", h.orchURL)
	_, respdata, err := orch.apiGet(ctx, h.orchURL, vars["serviceName"]+"_serviceStatus")

	var serviceStatus ServiceInstanceGroupStatus
	err = json.Unmarshal(respdata, &serviceStatus)
	return serviceStatus, err
}
