package app

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

// Create Cluster Provider and cluster-sync-objects required for GitOps
func (h *OrchestrationHandler) CreateClusterProvider(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	vars := mux.Vars(r)
	h.Vars = vars
	var jsonData ClusterProvider
	h.InitializeResponseMap()

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&jsonData)
	if err != nil {
		log.Error("Failed to parse json: %s", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Create Cluster Provider
	cp := ClusterProvider{
		Metadata: apiMetaData{
			Name:        jsonData.Metadata.Name,
			Description: jsonData.Metadata.Description,
		},
	}

	jsonLoad, _ := json.Marshal(cp)
	url := "http://" + h.MiddleendConf.Clm + "/v2/cluster-providers"
	resp, err := h.apiPost(ctx, jsonLoad, url, jsonData.Metadata.Name+"_cp")
	if err != nil {
		log.Errorf("Encountered error while creating cluster provider: %s", jsonData.Metadata.Name)
		w.WriteHeader(resp.(int))
		return
	}

	clusterProvider := jsonData.Metadata.Name

	// Create cluster-sync-object, if required payload available
	if jsonData.Spec.GitEnabled && len(jsonData.Spec.Kv) > 0 {
		var kvinfo []map[string]interface{}
		for _, kvpair := range jsonData.Spec.Kv {
			log.Info("kvpair", log.Fields{"kvpair": kvpair})
			v, ok := kvpair["gitType"]
			if ok {
				gitType := map[string]interface{}{
					"gitType": fmt.Sprintf("%v", v)}
				kvinfo = append(kvinfo, gitType)
			}
			v, ok = kvpair["gitToken"]
			if ok {
				gitToken := map[string]interface{}{
					"gitToken": fmt.Sprintf("%v", v)}
				kvinfo = append(kvinfo, gitToken)
			}
			v, ok = kvpair["repoName"]
			if ok {
				repoName := map[string]interface{}{
					"repoName": fmt.Sprintf("%v", v)}
				kvinfo = append(kvinfo, repoName)
			}
			v, ok = kvpair["userName"]
			if ok {
				userName := map[string]interface{}{
					"userName": fmt.Sprintf("%v", v)}
				kvinfo = append(kvinfo, userName)
			}
			v, ok = kvpair["branch"]
			if ok {
				branch := map[string]interface{}{
					"branch": fmt.Sprintf("%v", v)}
				kvinfo = append(kvinfo, branch)
			}
		}
		jsonData.Spec.Kv = kvinfo
		jsonData.Metadata.Name = "GitObjectMyRepo"
		jsonLoad, _ := json.Marshal(jsonData)
		url := "http://" + h.MiddleendConf.Clm + "/v2/cluster-providers/" + clusterProvider + "/cluster-sync-objects"
		resp, err := h.apiPost(ctx, jsonLoad, url, jsonData.Metadata.Name+"_cp")
		if err != nil {
			log.Errorf("Encountered error while creating cluster sync object for clusterprovider: %s", jsonData.Metadata.Name)
			w.WriteHeader(resp.(int))
			return
		}
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(h.response.payload[clusterProvider+"_cp"])
}

// GetClusters get an a array of all the cluster providers and the clusters within them
func (h *OrchestrationHandler) GetClusters(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	vars := mux.Vars(r)
	h.Vars = vars
	h.InitializeResponseMap()
	dashboardClient := DashboardClient{h}
	retcode := dashboardClient.getAllClusters(ctx)
	if retcode != nil {
		if intval, ok := retcode.(int); ok {
			log.Infof("Failed to get clusterdata : %d", intval)
			w.WriteHeader(intval)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			errMsg := string(h.response.payload[h.response.lastKey]) + h.response.lastKey
			w.Write([]byte(errMsg))
		}
		return
	}

	var retval []byte
	retval, err := json.Marshal(h.ClusterProviders)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(retval)
}

// Delete Cluster Provider and cluster-sync-objects
func (h *OrchestrationHandler) DeleteClusterProvider(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	h.Vars = mux.Vars(r)
	h.InitializeResponseMap()
	// Fetch cluster sync object
	url := "http://" + h.MiddleendConf.Clm + "/v2/cluster-providers/" + h.Vars["clusterProvider"] + "/cluster-sync-objects"
	resp, data, err := h.apiGet(ctx, url, h.Vars["clusterProvider"])
	if err != nil {
		log.Errorf("Encountered error while fetching cluster sync object for clusterprovider: %s", h.Vars["clusterProvider"])
		w.WriteHeader(resp.(int))
		return
	}
	if resp != http.StatusOK {
		log.Errorf("Encountered error while fetching cluster sync object for clusterprovider: %s", h.Vars["clusterProvider"])
		w.WriteHeader(resp.(int))
		return
	}
	var jsonData []ClusterProvider
	json.Unmarshal(data, &jsonData)
	log.Info("clustersyncobjects: %+v", jsonData)

	// Delete cluster sync object
	if len(jsonData) > 0 && len(jsonData[0].Spec.Kv) != 0 {
		url := "http://" + h.MiddleendConf.Clm + "/v2/cluster-providers/" + h.Vars["clusterProvider"] + "/cluster-sync-objects/" + "GitObjectMyRepo"
		resp, err := h.apiDel(ctx, url, h.Vars["clusterProvider"])
		if err != nil {
			log.Errorf("Encountered error while deleting cluster sync object for clusterprovider: %s", h.Vars["clusterProvider"])
			w.WriteHeader(resp.(int))
			return
		}
		if resp != nil && resp.(int) != http.StatusNoContent {
			log.Errorf("Encountered error while deleting cluster sync object for clusterprovider: %s", h.Vars["clusterProvider"])
			w.WriteHeader(resp.(int))
			return
		}
	}

	// Delete cluster provider
	url = "http://" + h.MiddleendConf.Clm + "/v2/cluster-providers/" + h.Vars["clusterProvider"]
	resp, err = h.apiDel(ctx, url, h.Vars["clusterProvider"])
	if err != nil {
		log.Errorf("Encountered error while deleting clusterprovider: %s", h.Vars["clusterProvider"])
		w.WriteHeader(resp.(int))
		return
	}
	if resp != nil && resp.(int) != http.StatusNoContent {
		log.Errorf("Encountered error while deleting clusterprovider: %s", h.Vars["clusterProvider"])
		w.WriteHeader(resp.(int))
		return
	}
	w.WriteHeader(resp.(int))
}

// DeleteCluster deletes a cluster
func (h *OrchestrationHandler) DeleteCluster(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	h.Vars = mux.Vars(r)
	h.InitializeResponseMap()

	clusterProvider := h.Vars["clusterProvider"]
	cluster := h.Vars["cluster"]

	err := h.CleanMonitor(ctx, clusterProvider, cluster)
	if err != nil {
		log.Warnf("failed to clean MonitorApp for cluster %s, with error %q", cluster, err)
	}

	// Delete cluster
	url := fmt.Sprintf("http://%s/v2/cluster-providers/%s/clusters/%s", h.MiddleendConf.Clm, clusterProvider, cluster)
	resp, err := h.apiDel(ctx, url, cluster)
	if err != nil {
		log.Errorf("Encountered error while deleting cluster: %s %s", clusterProvider, cluster)
		w.WriteHeader(resp.(int))
		return
	}
	if resp != nil && resp.(int) != http.StatusNoContent {
		log.Errorf("Encountered error while deleting cluster: %s %s", clusterProvider, cluster)
		w.WriteHeader(resp.(int))
		return
	}

	w.WriteHeader(resp.(int))
}
