package utils

import (
	"encoding/json"
)

func ConvertType(in, out interface{}) error {
	b, err := json.Marshal(in)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, out)
}

func StructToMap(s interface{}) (map[string]interface{}, error) {
	var m map[string]interface{}
	return m, ConvertType(s, &m)
}
